using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //properties publicas
    public float velocityX = 15;
    public float jumpForce = 40;
    //componenetes
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Animator animator;
    private bool slide = false;
    
    // constantes
    private const int ANIMATION_RUN = 0;
    private const int ANIMATION_JUMP= 1;
    private const int ANIMATION_SLIDE= 2;

    private const int LAYER_GROUND = 10;
    private int Enemies = 0;

    private const string TAG_ENEMY = "Enemy";
    private const string TAG_PLAYER = "Player";
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(velocityX, rb.velocity.y);
        ChangeAnimation(ANIMATION_RUN); 
        
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocityX, rb.velocity.y);
            sr.flipX = false;
            ChangeAnimation(ANIMATION_RUN);
        }
        
        if (Input.GetKey(KeyCode.X))
        {
            ChangeAnimation(ANIMATION_SLIDE);
            slide = true;
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            velocityX += 1.5f;
            rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse); // salta
            ChangeAnimation(ANIMATION_JUMP); // saltar
        }
        if(Enemies >= 10)
        {
            velocityX = 0;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag(TAG_ENEMY))
        {
            if(slide)
            {
                Destroy(collision.gameObject);
                Enemies ++;
                velocityX += 1.5f;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }


    private void ChangeAnimation(int animation)
    {
        animator.SetInteger("Estado", animation);
    }
}
