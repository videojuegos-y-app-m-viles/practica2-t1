using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour
{
    public Transform pF;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var x = pF.position.x + 0.4f;
        var y = pF.position.y + 0.6f;
        transform.position = new Vector3(x, y, transform.position.z);
    }
}
